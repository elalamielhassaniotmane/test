package domain.manager;

import domain.exceptions.InputException;
import domain.models.PAndLImpl;
import domain.models.interfaces.PAndL;
import domain.service.interfaces.FileService;
import domain.service.interfaces.InputService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PAndLManagerTest {
    @Mock
    private InputService inputService;
    @Mock
    private FileService fileService;
    @Mock
    private File file1;
    @Mock
    private File file2;

    private PAndLManager pAndLManager ;

    @Before
    public void setUp() throws Exception {
        pAndLManager = new PAndLManager(inputService,fileService);
    }

    @Test(expected =  InputException.class  )
    public void start_process_valid_coe() {
        pAndLManager.startProcess(LocalDate.now(),10,200,"");
    }

    @Test(expected =  InputException.class  )
    public void start_process_empty_file() {
        Mockito.when(fileService.filterFiles(Mockito.anyString())).thenReturn(Arrays.asList(file1,file2));
        Mockito.when(inputService.getPAndLData(Mockito.any())).thenReturn(Collections.emptyList());
        pAndLManager.startProcess(LocalDate.now(),10,99,"");
    }

    @Test(expected =  InputException.class  )
    public void start_process_valid_nth() {
        pAndLManager.startProcess(LocalDate.now(),10,1,"");
    }


}