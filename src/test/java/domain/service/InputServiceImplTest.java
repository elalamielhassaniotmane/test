package domain.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import data.file.csv.entity.PAndLEntityImpl;
import data.file.csv.services.interfaces.CsvService;
import domain.exceptions.DataException;
import domain.models.interfaces.PAndL;
import domain.service.interfaces.InputService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class InputServiceImplTest {
    @Mock
    private CsvService csvService;
    @Mock
    File file;
    private InputService inputService;

    private PAndLEntityImpl pAndLEntity1;
    private PAndLEntityImpl pAndLEntity2;
    private PAndLEntityImpl pAndLEntity3;
    private PAndLEntityImpl pAndLEntity4;



    @Before
    public void setUp() throws Exception {
        inputService= new InputServiceImpl(csvService);
        pAndLEntity3 = new PAndLEntityImpl();
        pAndLEntity4 = new PAndLEntityImpl();
        pAndLEntity3.setAmount("11");
        pAndLEntity4.setAmount("22");
    }

    @Test(expected = DataException.class)
    public void get_p_and_l_data_null() {
        Mockito.when(csvService.readCsvFile(file,PAndLEntityImpl.class))
                .thenReturn(Arrays.asList(pAndLEntity1,pAndLEntity2));
        inputService.getPAndLData(file);
    }
    @Test
    public void get_p_and_l_data() {
        Mockito.when(csvService.readCsvFile(file,PAndLEntityImpl.class))
                .thenReturn(Arrays.asList(pAndLEntity3,pAndLEntity4));
        List<PAndL> pAns = inputService.getPAndLData(file);
        assertEquals(2,pAns.size());
    }

}