import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import data.file.csv.services.CsvServiceImpl;
import data.file.csv.services.interfaces.CsvService;
import domain.manager.PAndLManager;
import domain.service.FileServiceImpl;
import domain.service.InputServiceImpl;
import domain.service.interfaces.FileService;
import domain.service.interfaces.InputService;

import java.time.LocalDate;

public class Application {

    public static void main(String[] args) {
        CsvMapper csvMapper = (CsvMapper) new CsvMapper()
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule());

        CsvService csvService = new CsvServiceImpl(csvMapper);
        InputService inputService = new InputServiceImpl(csvService);
        FileService fileServiceImpl = new FileServiceImpl();
        PAndLManager pAndLManager = new PAndLManager(inputService, fileServiceImpl);
        pAndLManager.startProcess(LocalDate.of(2019, 01, 1),
                12,
                84,
                "C:\\Users\\33618\\Desktop\\coding");
    }


}
