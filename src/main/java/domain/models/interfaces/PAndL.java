package domain.models.interfaces;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface PAndL {
    LocalDate getDate();

    BigDecimal getAmount();
}
