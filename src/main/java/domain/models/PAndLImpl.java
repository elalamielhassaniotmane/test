package domain.models;

import domain.models.interfaces.PAndL;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

public class PAndLImpl implements PAndL {

    private LocalDate date;
    private BigDecimal amount;

    public static Builder builder() {
        return new Builder();
    }

    public LocalDate getDate() {
        return date;
    }

    private void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    private void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public static class Builder {
        private LocalDate date;
        private BigDecimal amount;

        public Builder setDate(LocalDate date) {
            this.date = date;
            return this;
        }

        public Builder setAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public Builder setAmount(String amount) {
            this.amount = new BigDecimal(Optional.ofNullable(amount).map(s -> s.trim()).orElse("0"));
            return this;
        }

        public PAndLImpl build() {
            PAndLImpl PAndLImpl = new PAndLImpl();
            PAndLImpl.setAmount(this.amount);
            PAndLImpl.setDate(this.date);
            return PAndLImpl;
        }
    }
}
