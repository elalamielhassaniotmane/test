package domain.manager;

import domain.exceptions.InputException;
import domain.exceptions.InputExceptionType;
import domain.models.interfaces.PAndL;
import domain.service.interfaces.FileService;
import domain.service.interfaces.InputService;

import java.io.File;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PAndLManager {
    private final InputService inputService;
    private final FileService fileService;

    public PAndLManager(InputService inputService,
                        FileService fileService) {
        this.inputService = inputService;
        this.fileService = fileService;
    }

    public void startProcess(LocalDate currentDate, int depth, int coe, String path) {
        validateCeo(coe);
        List<File> files = fileService.filterFiles(path);
        int nth = calculateNTh(depth, coe);
        files.forEach(file -> {
            double var = calculateVar(currentDate, depth, nth, file);
            System.out.println(var);

        });

    }

    private void validateCeo(int coe) {
        if (coe < 0 || coe > 100) {
            throw new InputException(InputExceptionType.ERROR_IN_INPUT);
        }
    }

    private double calculateVar(LocalDate currentDate, int depth, int nth, File file) {
        List<PAndL> pAndLs = inputService.getPAndLData(file);
        List<PAndL> sortedPAndLs = pAndLs.stream()
                .filter(pAndL -> (pAndL.getDate().isBefore(currentDate)) || pAndL.getDate().isEqual(currentDate))
                .sorted(Comparator.comparing(PAndL::getAmount).reversed())
                .limit(depth)
                .collect(Collectors.toList());

        if (nth > sortedPAndLs.size()) {
            throw new InputException(InputExceptionType.ERROR_IN_INPUT);
        }
        return sortedPAndLs.get(nth - 1).getAmount().doubleValue();

    }

    private int calculateNTh(int depth, int coe) {
        int nTH = Math.round((float) depth * (float) coe / (float) 100);
        if (nTH < 1) {
            throw new InputException(InputExceptionType.ERROR_IN_INPUT);
        }
        return nTH;
    }

}
