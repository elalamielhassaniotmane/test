package domain.service;

import data.exception.FileException;
import data.exception.FileExceptionType;
import domain.service.interfaces.FileService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileServiceImpl implements FileService {
    public static final String HISTORICAL_PN_L_CSV = "HISTORICAL_PnL_.*.csv";
    private Pattern pattern = Pattern.compile(HISTORICAL_PN_L_CSV);

    public List<File> filterFiles(String path) {
        try (Stream<Path> walk = Files.walk(Paths.get(path))) {
            return walk.map(Path::toString)
                    .map(File::new)
                    .filter(f -> f.getName()
                            .matches(pattern.pattern()
                            )
                    )
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new FileException(FileExceptionType.ERROR_READING_FILE);
        }
    }
}
