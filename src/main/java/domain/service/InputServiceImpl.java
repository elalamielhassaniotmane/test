package domain.service;

import data.file.csv.entity.PAndLEntityImpl;
import data.file.csv.services.interfaces.CsvService;
import domain.mappers.PAndLMapper;
import domain.models.interfaces.PAndL;
import domain.service.interfaces.InputService;

import java.io.File;
import java.util.List;

public class InputServiceImpl implements InputService {
    private final CsvService csvService;


    public InputServiceImpl(CsvService csvService) {
        this.csvService = csvService;
    }

    public List<PAndL> getPAndLData(File file) {
        List<PAndLEntityImpl> pAndLEntities = csvService.readCsvFile(file, PAndLEntityImpl.class);
        return PAndLMapper.from(pAndLEntities);
    }

}
