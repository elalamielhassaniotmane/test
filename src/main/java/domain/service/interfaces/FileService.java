package domain.service.interfaces;

import java.io.File;
import java.util.List;

public interface FileService {
    List<File> filterFiles(String path);
}
