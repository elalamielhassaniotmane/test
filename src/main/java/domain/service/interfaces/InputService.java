package domain.service.interfaces;

import domain.models.interfaces.PAndL;

import java.io.File;
import java.util.List;

public interface InputService {
    List<PAndL> getPAndLData(File file);
}
