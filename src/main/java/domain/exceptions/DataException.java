package domain.exceptions;

public class DataException extends RuntimeException {
    private final DataExceptionType dataExceptionType;

    public DataException(DataExceptionType errorData) {
        this.dataExceptionType = errorData;
    }

    @Override
    public String getMessage() {
        switch (dataExceptionType) {
            case ERROR_DATA:
                return "Error in input data";
            default:
                return "Unknown Error";
        }
    }
}
