package domain.exceptions;

public class InputException extends RuntimeException {
    private final InputExceptionType inputExceptionType;

    public InputException(InputExceptionType inputExceptionType) {
        this.inputExceptionType = inputExceptionType;
    }

    @Override
    public String getMessage() {
        switch (inputExceptionType) {
            case ERROR_IN_INPUT:
                return "Error in the input";
            default:
                return "unknown error";
        }
    }
}
