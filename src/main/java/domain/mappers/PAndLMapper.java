package domain.mappers;

import data.file.csv.entity.PAndLEntityImpl;
import domain.exceptions.DataException;
import domain.exceptions.DataExceptionType;
import domain.models.PAndLImpl;
import domain.models.interfaces.PAndL;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PAndLMapper {
    private PAndLMapper() {
    }

    public static PAndL from(PAndLEntityImpl pAndLEntity) {
        return Optional.ofNullable(pAndLEntity)
                .map(pAndLEntity1 -> {
                            return PAndLImpl.builder()
                                    .setAmount(pAndLEntity1.getAmount())
                                    .setDate(pAndLEntity1.getDate())
                                    .build();
                        }
                ).orElseThrow(() -> new DataException(DataExceptionType.ERROR_DATA));
    }

    public static List<PAndL> from(List<PAndLEntityImpl> pAndLEntity) {
        return pAndLEntity.stream()
                .map(PAndLMapper::from)
                .collect(Collectors.toList());
    }
}
