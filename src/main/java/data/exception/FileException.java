package data.exception;

public class FileException extends RuntimeException {
    private final FileExceptionType fileExceptionType;

    public FileException(FileExceptionType fileExceptionType) {
        this.fileExceptionType = fileExceptionType;
    }

    @Override
    public String getMessage() {
        switch (fileExceptionType) {
            case ERROR_READING_FILE:
                return "Error reading file";
            default:
                return "Unknown Error";
        }
    }
}
