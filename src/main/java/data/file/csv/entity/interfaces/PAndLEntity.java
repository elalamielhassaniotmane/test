package data.file.csv.entity.interfaces;

import java.time.LocalDate;

public interface PAndLEntity {
    LocalDate getDate();

    String getAmount();

}
