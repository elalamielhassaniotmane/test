package data.file.csv.services;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import data.exception.FileException;
import data.exception.FileExceptionType;
import data.file.csv.services.interfaces.CsvService;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class CsvServiceImpl implements CsvService {
    private final CsvMapper csvMapper;

    public CsvServiceImpl(CsvMapper csvMapper) {
        this.csvMapper = csvMapper;
    }

    @Override
    public <T> List<T> readCsvFile(File file, Class<T> cls) {
        CsvSchema csvSchema = csvMapper.schemaFor(cls).withColumnSeparator(';');
        try {
            return (List<T>) csvMapper.readerFor(cls)
                    .with(csvSchema)
                    .readValues(file)
                    .readAll();
        } catch (IOException e) {
            throw new FileException(FileExceptionType.ERROR_READING_FILE);
        }
    }
}
