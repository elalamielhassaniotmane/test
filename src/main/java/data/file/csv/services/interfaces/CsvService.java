package data.file.csv.services.interfaces;

import java.io.File;
import java.util.List;

public interface CsvService {
    <T> List<T> readCsvFile(File file, Class<T> cls);
}
